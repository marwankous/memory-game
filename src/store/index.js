import { createStore } from "vuex";
import axios from "axios";

const debug = process.env.NODE_ENV !== "production";

export default createStore({
  state() {
    return {
      pairs: 10,
      matches: 0,
      remainingPairs: 10,
      score: 0,
      timeLeft: 10 * 8,
      total: 10 * 8,
      start: false,
      finish: false,
      play: false,
      initialState: null,
      immInitialState: null,
      percentage: 100,
      inter: null,
      randomCards: null,
    };
  },
  mutations: {
    incrementPairs(state) {
      state.pairs++;
    },
    incrementMatches(state) {
      state.matches++;
    },
    incrementScore(state) {
      state.score = state.score + 100;
    },
    reducePairs(state) {
      state.remainingPairs--;
    },
    isStart(state) {
      state.start = !state.start;
    },
    isFinish(state) {
      state.finish = !state.finish;
    },
    isPlay(state) {
      state.play = true;
    },
    initialState(state, payload) {
      state.initialState = payload;
    },
    immInitialState(state, payload) {
      state.immInitialState = payload;
    },

    computePercentage(state) {
      state.inter = setInterval(() => {
        if (state.timeLeft > 0 && state.play) {
          state.timeLeft -= 1;
          state.percentage = (state.timeLeft / state.total) * 100;
        } else if (state.timeLeft == 0) {
          state.start = !state.start;
          state.finish = !state.finish;
          clearInterval(state.inter);
        }
      }, 1000);
    },
    clear(state) {
      clearInterval(state.inter);
    },
  },
  actions: {
    countDown(state) {
      state.commit("computePercentage");
    },
    reset(store) {
      store.state.pairs = 10;
      store.state.matches = 0;
      store.state.remainingPairs = 10;
      store.state.score = 0;
      store.state.timeLeft = 10 * 8;
      store.state.start = false;
      store.state.play = false;
      store.state.finish = false;
      store.state.percentage = 100;
      store.commit(
        "initialState",
        JSON.parse(JSON.stringify(store.state.immInitialState)).sort(() => 0.5 - Math.random())
      );
    },
    getResults(store) {
      if (!sessionStorage.getItem("randomCard")) {
        axios.defaults.headers.common["x-api-key"] = "8801d9a9-19da-4884-a41c-30621ea6c271";
        axios
          .get("https://api.thecatapi.com/v1/images/search", {
            params: { limit: store.state.pairs, mime_types: "jpg,png" },
          })
          .then((response) => {
            store.state.randomCards = response.data
              .concat(response.data)
              .map((obj) => {
                return {
                  url: obj.url,
                  backUrl: "./assets/back.jpg",
                  blankUrl: "./assets/blank.jpg",
                  imageState: true,
                  matched: false,
                };
              })
              .sort(() => 0.5 - Math.random());
            sessionStorage.setItem("randomCard", JSON.stringify(store.state.randomCards));
            store.commit("immInitialState", JSON.parse(JSON.stringify(store.state.randomCards)));
            store.commit("initialState", store.state.randomCards);
          });
      } else {
        store.state.randomCards = JSON.parse(sessionStorage.getItem("randomCard")).sort(
          () => 0.5 - Math.random()
        );
        store.commit("immInitialState", JSON.parse(JSON.stringify(store.state.randomCards)));
        store.commit("initialState", store.state.randomCards);
      }
    },
  },
});
